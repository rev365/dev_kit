class Malbec
  def initialize(theme)
    @theme_path = Rails.root.join('themes', theme)
    theme_json = Rails.root.join('themes', theme, 'config.json')
    user_json = Rails.root.join('themes', 'config.json')

    if !File.directory?(@theme_path)
      abort "Theme doesn't exist"
    elsif !File.exist?(theme_json)
      abort "Theme config doesn't exist"
    elsif !File.exist?(user_json)
      abort "Site config doesn't exist."
    end

    file = File.read(theme_json)
    theme_config = JSON.parse(file)

    file = File.read(user_json)
    user_config = JSON.parse(file)

    @mykey = user_config['mykey']
    @user = user_config['user']
    @sitetoken = theme_config['token']
    @theme = theme_config['theme']
    @base_url = "#{theme_config['url']}/api/v1/themes/#{@theme}/files/"

    if @mykey.blank? || @user.blank? || @sitetoken.blank? || @theme.blank?
      abort "Missing config params."
    end

    @exts = %w[css scss js ttf woff eot]
    @dir = %w[layout templates sections snippets blocks assets]
    @download = %w[png ttf woff eot jpg jpeg gif svg]
  end

  def pull(overwright = false)
    response = make_request("",'get')
    files = JSON.parse(response.body)
    files = files['theme_files']
    files.each do |f|
      puts "#{f['name']}.#{f['ext']}"
      dir = f['type'].downcase
      unless dir == 'config' || dir == 'layout'
        dir = dir.pluralize
      end


      if !File.directory?("#{@theme_path}/#{dir}") && dir != 'robots' && dir != "sitemaps"
        Dir.mkdir "#{@theme_path}/#{dir}"
      end

      path = "#{@theme_path}/#{dir}/#{f['name']}.#{f['ext']}"
      if @download.include? f['ext']
        if (File.exist?(path) && overwright == true) || !File.exist?(path)
          open(path, 'wb') do |file|
            file << open(f['content']).read
          end
        end
      elsif dir != 'robots' && dir != "sitemaps"
        if (File.exist?(path) && overwright == true) || !File.exist?(path)
          File.open(path, "w+") do |file|
            file.write f['content']
          end
        end
      end
    end
  end

  def push
    image_ext = {
      'jpeg' => 'image/jpeg',
      'jpg' => 'image/jpeg',
      'png' => 'image/png',
      'gif' => 'image/gif',
      'svg' => 'image/svg+xml'
    }

    #dir = %w[assets]
    @dir.each do |d|
      path = "#{@them_path}/#{d}/"
      next if !File.directory?(path)
      type = d.capitalize.singularize
      Dir.foreach(path) do |item|
        next if item == '.' || item == '..' || item == '.keep' || item == '.DS_Store'
        file_parts = item.split('.')
        if @exts.include? file_parts[-2]
          ext = "#{file_parts[-2]}.#{file_parts[-1]}"
          file_parts.pop
          file_parts.pop
        else
          ext = file_parts.pop
        end

        file_name = file_parts.join('.')

        response = make_request("#{d.singularize}/#{file_name}",'get')

        if response.code == '404'
          puts "Adding file #{item}"
          form_data = [
            ['theme_file[theme_id]', "#{@theme}"],
            ['theme_file[name]', "#{file_name}"],
            ['theme_file[type]', "#{type}"],
            ['theme_file[ext]', "#{ext}"]
          ]
          if image_ext.include? ext
            form_data.push(['theme_file[image]', File.open("#{path}#{item}")])
            form_data.push(['theme_file[content]', ""])
          else
            form_data.push(['theme_file[content]', "#{ File.read("#{path}/#{item}")}"])
          end
          make_request("", 'post', form_data)
        elsif response.code == '200' && !(image_ext.include? ext)
          file = JSON.parse(response.body)
          puts "Checking file #{item}"
          if file["content"] != File.read("#{path}/#{item}")
            puts "Editing file #{item}"
            form_data = [
              ['theme_file[content]', "#{ File.read("#{path}/#{item}")}"],
            ]
            make_request(file['id'], 'post', form_data)
          end
        end
      end
    end
  end

  def make_request(uri = '', method = 'get', form_data = nil)
    url = URI("#{@base_url}#{uri}")
    http = Net::HTTP.new(url.host, url.port)
    #http.use_ssl = true
    if method == 'post'
      request = Net::HTTP::Post.new(url)
    else
      request = Net::HTTP::Get.new(url)
    end
    request['X-User-Token'] = @mykey
    request["X-Api-Token"] = @sitetoken
    request["X-User-Email"] = @user
    if method == 'post'
      request.set_form form_data, 'multipart/form-data'
    end
    http.request(request)
  end

end
