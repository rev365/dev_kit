namespace :theme do
  desc "TODO"
  task :push, [:theme] => :environment do |task, args|
    if args[:theme].blank?
      abort('Theme param is required')
    end
    malbec = Malbec.new(args[:theme])
    malbec.push
  end

  task :pull, [:theme, :overwrite] => :environment  do |task, args|
    if args[:theme].blank?
      abort('Theme param is required')
    end
    overwrite = false
    if args[:overwrite] == "true"
      overwrite = true
    end
    malbec = Malbec.new(args[:theme])
    malbec.pull(overwrite)
  end

end
